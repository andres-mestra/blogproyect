class AddColumnEstadoToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :estado, :string, default: "pendiente"
  end
end
