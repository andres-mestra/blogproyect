class AddColumnUrlImageToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :url_image, :string
  end
end
