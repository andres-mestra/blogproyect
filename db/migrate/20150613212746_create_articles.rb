class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :body
      t.integer :visitas , default: 0

      t.timestamps null: false
    end
  end
end
