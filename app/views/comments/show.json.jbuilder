json.id @comment.id
json.user_id @comment.user_id
json.article_id @comment.article_id
json.body @comment.body
json.created_at @comment.created_at
json.updated_at @comment.updated_at

json.user do
  if @comment.user.avatar.nil?
    json.avatar "false"
  else
  	json.avatar @comment.user.avatar
  end
  json.username @comment.user.username
end

