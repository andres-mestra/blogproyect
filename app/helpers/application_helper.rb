module ApplicationHelper
	def get_email_oauth
		if session[:omniauth_data]

			session[:omniauth_data][:email]
		else
			""
		end
	end


     #crea las targetas de los articulos en index
	def post_article (article)

		fecha_creado = formato_fecha(article.created_at)
     
	    html = "<a href = '/articles/#{article.url}' class='z-depth-2'>"\
	           "<div class='card medium'>"\
	                "<div class='card-image waves-effect waves-block waves-light'>"\
		                "<img class='image-post' style='background-image: url(#{article.url_image});'/>"\
		            "</div>"\
		    	    "<div class='card-content'>"\
		    	        "<h5 class='grey-text text-darken-4 card-titulo'>#{article.title}</h5>"\
		    	    "</div>"\
		    	    "<div class='card-datos center-align'><strong>#{article.user.username} - #{fecha_creado}</strong></div>"\
		    	"</div>"\
	            "</a>"

	    html.html_safe	
	end

	def encabezado_articulo(url_image, titulo_articulo, opciones = {})
     
        clase = opciones[:class]
        clase_cont = opciones[:class_cont]
		html = "<div class='#{clase_cont} center-xs middle-xs'>"\
		            "<h3>#{titulo_articulo}</h3>"\
			        "<img class='#{clase}' style='background-image: url(#{url_image});'/>"\
		       "</div>"
	    
	    html.html_safe

	end


	def slider_article(article, opciones= {})
		id_image = opciones[:id]
		caption_text = ""

		#esta condicion es para cambiar la clase que ejecuta el ejecto del texto en el slider
		if (article.id.to_i % 2) == 00
			caption_text = "center-align"
	    elsif (article.id.to_i % 3) == 0
	    	caption_text = "right-align"
	    else 
	    	caption_text ="left-align "
	    end
		  html = "<li><a href='articles/#{article.url}'>"\
		            "<img src='#{article.url_image}' style='background-image:url(#{article.url_image});'/>"\
		                "<div class='caption #{caption_text}'>"\
		                   "<h3>#{article.title}</h3>"\
		                "</div>"\
		          "</a></li>"

        html.html_safe
	end

    #crea el formato de la  fecha
	def formato_fecha(fecha)
       dia = fecha.time.strftime('%d')
       mes = fecha.time.strftime('%b')
       año = fecha.time.strftime('%Y')

       return "#{dia} #{mes} #{año}"
	end
end
