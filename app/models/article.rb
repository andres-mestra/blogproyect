class Article < ActiveRecord::Base
    include AASM

	belongs_to :user
    has_many :comments
    has_many :has_categories
    has_many :categories, through: :has_categories

    validates :title, presence: true, uniqueness: true
	validates :body, presence: true, length: { minimum: 20}
    validates :image, presence: true

    #before_create: esta asignando un valor por defecto al campo de visitas de cada articulo, antes de que sea creado.
    #puedo tambien usar before_save(after_save) , before_validation, pero estos metodos solo estan para el modelo.
    before_create :set_visitas
	after_create :update_url
    before_update :asignar_url
    #guarda las categorias
    after_create :save_categories
    #borra las categorias
    after_destroy :delet_comentarios

     
    #busca los articulos publicados
    scope :publicados, ->{ where(estado: "publicado")}
     #busca los articulos publicados
    scope :pendientes, ->{ where(estado: "pendiente")}
    #busca los ultimos articulos creados por fecha y limita la busqueda a 10
    scope :ultimos, ->{ order("created_at DESC")}
    #busca todos los articulos creados hace un mes
    scope :ultimo_mes, ->{ where(created_at: (Time.now.midnight - 30.day)..Time.now.midnight + 1.day)}
    #busca los articulos con mas de 15 visitas y limita la busqueda a 20
    scope :mas_visitas, ->{ where('visitas >= 15').limit(20)}

    #busca los articulos no publicados de un usuario
    def self.mis_art_pendientes(usuario)
        Article.where( user_id: usuario ).pendientes.ultimos
    end
     #busca los articulos publicados de un usuario
    def self.mis_art_publicados(usuario)
        Article.where( user_id: usuario ).publicados.ultimos
    end

    #resive las categorias de un articulo articulo
    def categories=(value)
        @categories = value
    end
    #resive los comentarios de los articulos que se borran
    def comments=(value)
        @comentarios = value
    end
    



     #metodo para contar las visitas de los articulos, este metodo lo esta usando en el controlador
    #la accion show
    def update_visitas
        self.update(visitas: self.visitas + 1)
    end


    aasm column: "estado" do
        state :pendiente, initial: true
        state :publicado


        event :publicar do
            transitions from: :pendiente, to: :publicado
        end

        event :nopublicar do
            transitions from: :publicado, to: :pendiente
        end
    end


    #redefinimos el metodo to_param
    def to_param
        url
    end


    private
    
    def save_categories
        unless @categories.nil?
            @categories.each do |category_id| 
               HasCategory.create(category_id: category_id,article_id: self.id) 
            end   
        end   
    end

    def delet_comentarios
       unless @comentarios.nil?
           @comentarios.each do |comentario|
               Comment.find(comentario.id).destroy
           end        
        end
    end

    def set_visitas
        #colocamos un valor por defecto al campo visitas de la tabla articles
        # ||= este operador es: si es igual o nulo
        self.visitas = 0;
    end

    def asignar_url
        self.url = "#{ title.parameterize }"
    end

    def update_url
        update_attributes url: asignar_url
    end
end
