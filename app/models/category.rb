class Category < ActiveRecord::Base
    validates :name , presence: true
    has_many :has_categories
    has_many :articles , through: :has_categories

    after_create :update_url
    before_update :asignar_url

     #redefinimos el metodo to_param
    def to_param
        url
    end

    def asignar_url
        self.url = "#{ name.parameterize }"
    end

    def update_url
        update_attributes url: asignar_url
    end
end
