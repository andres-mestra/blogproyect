class User < ActiveRecord::Base

  #validate :validacion_personalizada, on: :create
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, omniauth_providers: [:facebook, :twitter]

  validates :username, presence: true, uniqueness: true,
            length:{in: 5..20, too_short:"Tiene que tener minimo 5 caracteres", too_long:"Tienen que tener maximo 20 caracteres"},
            format: {with: /([A-Za-z0-9\-\_]+)/, message: "El nombre de usuario solo puede tener letras, numeros y guiones"}


  has_many :articles
  has_many :comments

  include PermissionsConcern

  def self.find_or_create_by_omniauth(auth)

  	#buscamos si alguien ya se logeo con esos datos sino lo creamos 
  	usuario = User.where(provedor: auth[:provedor], uid: auth[:uid]).first

  	if  usuario.nil?
  		usuario = User.create(
  			username: auth[:username],
  			email: auth[:email],
        avatar: auth[:avatar],
  			uid: auth[:uid],
  			provedor: auth[:provedor],
  			password: Devise.friendly_token[0,20]
  		)	
  	end

    return usuario
  	
  end

 
  private
=begin
  def validacion_personalizada
    if true

    else
       errors.add(:username, "Tu")
    end
  end
=end

end
