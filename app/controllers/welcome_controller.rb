class WelcomeController < ApplicationController
  before_action :authenticate_editor!, only: [:perfil]
  before_action :authenticate_admin!, only: [:publicar]
  def index
  	@articles = Article.publicados.ultimos
  end
  
  def publicar
  	@articles = Article.paginate(page: params[:page],per_page:5).ultimos
  end

  def perfil
  	@mis_art_pen = Article.mis_art_pendientes(current_user.id)
    @mis_art_publi = Article.paginate(page: params[:page],per_page:5).mis_art_publicados(current_user.id)
  end

end
