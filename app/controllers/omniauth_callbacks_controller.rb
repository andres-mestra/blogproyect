class OmniauthCallbacksController < ApplicationController
	 
	 #sse encarga de el login con facebook
	def facebook
		#resivo los datos de ouniauth me retorna 
		auth = request.env["omniauth.auth"]
		data = {
			username: auth.info.name,
			email: auth.info.email,
			avatar: auth.info.image,
			provedor: auth.provider,
			uid: auth.uid
		}

		
		@usuario = User.find_or_create_by_omniauth(data)

		if @usuario.persisted?
            sign_in_and_redirect @usuario, event: :authentication
	    else
	    	session[:omniauth_errors] = @usuario.errors.full_messages.to_sentence unless @usuario.save

	    	session[:omniauth_data] = data

	    	redirect_to new_user_registration_url
		end
	end


	def twitter

		#resivo los datos que ouniauth me retorna 
		auth = request.env["omniauth.auth"]
		data = {
			username: auth.info.nickname,
			email: auth.info.nickname + "@twitter.com",
			avatar: auth.info.image,
			provedor: auth.provider,
			uid: auth.uid
		}
           

		@usuario = User.find_or_create_by_omniauth(data)

		if @usuario.persisted?
            sign_in_and_redirect @usuario, event: :authentication
	    else
	    	session[:omniauth_errors] = @usuario.errors.full_messages.to_sentence unless @usuario.save

	    	session[:omniauth_data] = data

	    	redirect_to new_user_registration_url
		end
		
	end

	
end