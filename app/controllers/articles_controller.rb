class ArticlesController < ApplicationController
=begin
  before_action : este ejecuta un codigo antes que otra accion que le indiquemos, tambien esta after_action, que seria despues.
  authenticate_usuer! : es un metodo de devise que autentica si estamos logiados y si no lo estamos, no redirige a la pagina
  de login, este recive un un parametro en donde indicamos en que acciones se realizara la autenticacion(only:) o en cuales no(exept:).
=end

  #para el controlador hay solo, before_action
  before_action :authenticate_user!, except: [:show,:index]
  #set_article :me permite poner codigo que pueden usar solo cualquier accion de este controlador, puesto que esta en private
  before_action :set_article, only: [:show, :edit, :update, :destroy, :publicar]
  before_action :authenticate_editor!, only: [:new,:create,:update, :edit]
  before_action :authenticate_admin!, only: [:destroy,:publicar]




  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.paginate(page: params[:page],per_page:3).publicados.ultimos
  end


  # GET /articles/1
  # GET /articles/1.json
  def show
    @article.update_visitas
    @comment = Comment.new
  end

  # GET /articles/new
  def new
    @article = Article.new
    @categories = Category.all
  end

  # GET /articles/1/edit
  def edit
    @articulo = params[:id]
  end


  # POST /articles
  # POST /articles.json
  def create

    @article = current_user.articles.new(article_params)
   
    unless article_params[:image].nil?
      #sube la portada del articulo
      subir = Hash.new
      subir = Cloudinary::Uploader.upload(article_params[:image],
          :use_filename => true,
            :allowed_formats => [ 'gif' , 'png', 'jpg'],
          :tags => "portada articulos")

        @article[:image] = subir["public_id"]
        @article[:url_image] = subir["url"]
    end

    #ejecuta el metodo categories que esta en el modelo y le pasa params => categories
    @article.categories = params[:categories]

     if @article.save
        redirect_to @article 
       else
        render :new 
    end


  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
      if @article.update(article_params)
        redirect_to @article
      else
        render :edit
      end
  end



  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    #elimina de cloudinary la imagen del articulo
    eliminar_img = {};
    eliminar_img = Cloudinary::Uploader.destroy(@article.image)
    
    #eleminar los comentarios del articulo de la base de datos
    
    @article.comments = @article.comments
  

    @article.destroy
    redirect_to articles_path
  end

  def publicar
    @article.publicar!
    redirect_to @article
  end

  private


    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find_by url: params[:id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :body, :visitas, :categories, :markup_body,:image, :url_image)
    end
end
