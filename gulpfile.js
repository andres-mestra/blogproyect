

//estas son las dependencias de gulp que necesitamos
var gulp = require('gulp'),
    concat = require('gulp-concat'),//concatena contenido de archivos 
    uglify = require('gulp-uglify'),// optimiza archivos javascritp
    stylus = require('gulp-stylus'),//usa staylus
    nib = require('nib'), //usa nib para el uso de stylus
    watch = require('gulp-watch'); //esta pendiente de cambios en archvos 
 

 
//esta tarea concatena todos los archivos .styl  y los pone en  estylos.styl
gulp.task('concatenar', function() {
    gulp.src('estilos/*.styl')
    .pipe(concat('estilos.styl'))
    .pipe(gulp.dest('estilos/concatenacion/'))
});

//esta tarea convierte todos los archivos .styl a css
gulp.task('css',['concatenar'] ,function () {
  gulp.src('estilos/concatenacion/estilos.styl')
    .pipe(stylus(
      {use: nib()
      }))
  
    .pipe(gulp.dest('app/assets/stylesheets/'))
});


//esta tara esta escuchando cuando halla algun cambia en el archivo estilos.styl de la tarea css
gulp.task('watchcss', function () {
  gulp.watch(['estilos/*.styl'], ['css']);
});
 
gulp.task('estilos', ['css', 'watchcss','concatenar']);


